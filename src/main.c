/*
******************************************************************************
File:     main.c
Info:     Generated by Atollic TrueSTUDIO(R) 9.2.0   2020-05-08

The MIT License (MIT)
Copyright (c) 2018 STMicroelectronics

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

******************************************************************************
*/

/* Includes */
#include "stm32f0xx.h"

#include	"gpio.h"
#include	"keyboard.h"
#include	"tim.h"

/**
**===========================================================================
**
**  Abstract: main program
**
**===========================================================================
*/

//extern uint8_t num_but[ROW_NUM * COL_NUM];

void TIM6_DAC_IRQHandler() {
	TIM6->SR &= ~TIM_SR_UIF;
	GetAddresButton(NEXT_ROW);
}

void process_button(uint8_t cur_but) {
	switch (cur_but) {
		case 0 :
			//here process pressed button
		break;

		case 1 :
		break;

		case 2 :
		break;

		default :
		break;
	}
}

int main(void)
{
	InitGPIOC();	//blink
	InitKeyboard();	//keyboard
	tim6_init();
	uint8_t addres_but = 0;

	while (1)
	{
		addres_but = ReturnAddresNotProcessedButton();
		process_button(addres_but);
	}
}







