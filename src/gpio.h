/*
 * gpio.h
 *
 *  Created on: 8 ��� 2020 �.
 *      Author: Tugrik
 */

#ifndef GPIO_H_
#define GPIO_H_

#include	<stm32f0xx.h>

//typedef uint16_t quint16_t;

void InitGPIOB();
void InitGPIOC();

void SetValPin_PB(uint8_t pin_num, uint8_t val);
uint16_t ReadValPin_PB();
uint16_t ReadValPin(GPIO_TypeDef* port);


void SetValPin(GPIO_TypeDef* port, uint8_t pin_num, uint8_t val);

#endif /* GPIO_H_ */
