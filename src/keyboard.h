/*
 * keyboard.h
 *
 *  Created on: 8 ��� 2020 �.
 *      Author: Tugrik
 */

#ifndef KEYBOARD_H_
#define KEYBOARD_H_

#include	<stm32f0xx.h>
#include	"gpio.h"

#define	ROW_NUM	4
#define	COL_NUM	4

#define	NEXT_ROW	1

void InitKeyboard();

uint8_t PereborInput();
uint16_t GetVal();

uint8_t GetAddresButton(uint8_t st);

uint8_t ReturnAddresNotProcessedButton();
void ButtonProcessed(uint8_t num_but);

#endif /* KEYBOARD_H_ */
