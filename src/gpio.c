/*
 * gpio.c
 *
 *  Created on: 8 ��� 2020 �.
 *      Author: Tugrik
 */
#include	"gpio.h"

void InitGPIOB() {
	RCC->AHBENR |= RCC_AHBENR_GPIOBEN;
	GPIOB->MODER |= GPIO_MODER_MODER0_0 | GPIO_MODER_MODER1_0 | GPIO_MODER_MODER2_0 | GPIO_MODER_MODER3_0;	//output
	GPIOB->MODER &= ~(GPIO_MODER_MODER4 | GPIO_MODER_MODER5 | GPIO_MODER_MODER6 | GPIO_MODER_MODER7);	//input
	GPIOB->PUPDR |= GPIO_PUPDR_PUPDR4_1 | GPIO_PUPDR_PUPDR5_1 | GPIO_PUPDR_PUPDR6_1 | GPIO_PUPDR_PUPDR7_1;
}

void InitGPIOC() {
	RCC->AHBENR |= RCC_AHBENR_GPIOCEN;
	GPIOC->MODER |= GPIO_MODER_MODER8_1 | GPIO_MODER_MODER9_1;
}

void SetValPin_PB(uint8_t pin_num, uint8_t val) {

//possible to write next code	//	GPIOB->BSRR = (1 << (pin_num + (val * 16)));
//possible to write next code	//	GPIOB->BSRR = (pin_num + (val * 16));

	if (val) {
		GPIOB->BSRR = pin_num;
	}
	else {
		GPIOB->BSRR = pin_num << 16;
	}
}

void SetValPin(GPIO_TypeDef* port, uint8_t pin_num, uint8_t val) {

}

uint16_t ReadValPin_PB() {
	return GPIOB->IDR;
}

uint16_t ReadValPin(GPIO_TypeDef* port) {
	return port->IDR;
}

