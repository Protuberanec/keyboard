/*
 * keyboard.c
 *
 *  Created on: 8 ��� 2020 �.
 *      Author: Tugrik
 */

#include	"keyboard.h"

typedef uint8_t quint8_t;

static uint8_t row[ROW_NUM];
static uint8_t col[COL_NUM];
static uint8_t buttons[ROW_NUM * COL_NUM + 1];

void InitKeyboard() {
	InitGPIOB();

	row[0] = GPIO_ODR_0;
	row[1] = GPIO_ODR_1;
	row[2] = GPIO_ODR_2;
	row[3] = GPIO_ODR_3;

	col[0] = GPIO_ODR_4;
	col[1] = GPIO_ODR_5;
	col[2] = GPIO_ODR_6;
	col[3] = GPIO_ODR_7;


	for (uint8_t i = 0; i < ROW_NUM * COL_NUM; i++) {
		buttons[i] = 0;
	}
}

/*	input	:
 * 	output	:	return set number string / row
 * 	descr	:
 * 	author	:
 * 	date	:	2020.05.08
 */
uint8_t PereborInput() {
	static uint8_t status = 0;
	SetValPin_PB(row[status], 0);
	status = ++status % ROW_NUM;
	SetValPin_PB(row[status], 1);

	return status;
}

/*	input	:
 * 	output	:	return set number string / row
 * 	descr	:
 * 	author	:
 * 	date	:	2020.05.08
 */

uint16_t GetVal() {
	return ReadValPin_PB();
}

/*
 *	main function to process data of pressed buttons...
 */
uint8_t GetAddresButton(uint8_t st) {
	uint8_t num_row = 1;
	if (st == 1) {
		num_row = PereborInput();
	}
	uint16_t num_col_bin = GetVal();

	num_col_bin = (num_col_bin & 0xF0) >> 4;

	uint8_t num_col_dec = 0;
	uint8_t pressed_key = 0;
	for (num_col_dec = 0; num_col_dec < 4; num_col_dec++) {
		if (num_col_bin & (1 << num_col_dec)) {
			buttons[num_row * 4 + num_col_dec] |= 0x01;
			pressed_key++;
		}
		else {
			if (buttons[num_row * 4 + num_col_dec] & 0x81)
				buttons[num_row * 4 + num_col_dec] = 0;
		}
	}

	return pressed_key;
}

uint8_t ReturnAddresNotProcessedButton() {
	uint8_t cur_but = 0;

	for (cur_but = 0; cur_but < COL_NUM * ROW_NUM; cur_but++) {
		if (buttons[cur_but] & (1 << 0) == 1) {
			if ((buttons[cur_but] & (1 << 7)) == 0) {
				ButtonProcessed(cur_but);
				break;
			}
		}
	}

	return cur_but;
}

void ButtonProcessed(uint8_t but_num) {
	buttons[but_num] |= 0x80;
}





